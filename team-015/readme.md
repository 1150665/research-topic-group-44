# Project Research Topic
### EDOM
# Introdução
Este documento tem como objetivo a apresentação de uma abordagem para efetuar validações de modelos diferente da apresentada na unidade curricular de Engenharia de Domínio (utilização do OCL).
Posto isto, a abordagem selecionada foi a apresentação de validações de modelos com recurso ao Modelling SDK da Microsoft e o Jetbrains MPS.

# Microsoft DSL SDK
## Validações Microsoft DSL SDK
Microsoft DSL SDK é um componente que foi desenvolvido pela Microsoft e pode ser utilizado no Visual Studio. Este recurso permite desenvolver um modelo rapidamente, na forma de uma linguagem específica de domínio (DSL).
No nosso projeto de pesquisa iremos abordar as validações sobre a DSL. 
Estas apresentam as seguintes características:
  - Escritas na linguagem C#
  - É possível indicar quais as classes a ser validadas.
  - Cada classe contém um ficheiro para definir as suas validações. 
  - Uma validação é definida por um método no ficheiro de domínio da classe ou das relações.

# Quando ocorrem as validações
Utilizando o Microsoft DSL SDK, podemos especificar quando é que pretendemos que as validações de determinada classe sejam executadas segundo as seguintes opções:

| Categoria | Execução |
| ------ | ------ |
| ValidationCategories.Menu | Validações são executadas quando o utilizador escolhe no menu o comando Validar. |
| ValidationCategories.Open |  Validações são executadas quando se abre o ficheiro da DSL. |
| ValidationCategories.Save |   Validações são executadas quando se fecha o ficheiro da DSL.Caso o ficheiro contenha erros o utilizador é informado, podendo cancelar o guardar do ficheiro. |
| ValidationCategories.Load |   Validações são executadas imediatamente após o ficheiro ser carregado. |
| ValidationCategories.Custom |  Método de validação personalizado. As validações poderão apenas ser executadas pelo código do programa. |



# Specify when the validation method should be executed (Example)
![S6](s6.png)

# Microsoft DSL SDK vs OCL
- End Date greater than Start Date in MindMap

- Must Have Title in MindMap

# End Date greater than Start Date in MindMap

![S8](s8.png)

# Must Have Title in MindMap

![S9](s9.png)

# Validações entre classes relacionadas

![S10](s10.png)

# Vantagens
- Microsoft DSL SDK
    - Mais fáceis de entender para os programadores devido ao facto de serem escritas em C#;
    - Mais organizadas devido ao facto de termos ficheiros de validações para cada Class;
    - Mais flexíveis já que podemos selecionar quais as classes que pretendemos aplicar as validações;
    - Permite especificar o momento em que serão efetuadas as validações;
- OCL
    - Menos linhas de código para efetuar validações;

# JetBrains MPS
## JetBrains MPS
- Sistema de meta-programação desenvolvido pela JetBrains.
- É uma ferramenta de design de DSLs.

## Validações em MPS
Em MPS, as validações são realizadas através de regras de Type System.
O Type System permite definir regras de validação através da atribuição de tipos aos nós dos modelos ou verificação de regras de validação.
Entre estas regras destacamos as seguintes:
- Inference Rules
- Comparison Rules
- Checking Rules
Cada regra é definida num ficheiro à parte e pode identificar três tipos de severidade: error, warning e info.

## Inference Rules
![S15](s15.png)
## Comparison Rules
![S16](s16.png)
## Checking Rules
![S17](s17.png)
## Checking Rules (Cont.)
![S18](s18.png)
## Referências
- https://docs.microsoft.com/en-us/visualstudio/modeling/validation-in-a-domain-specific-language?view=vs-2017&fbclid=IwAR0nybnMtquLHJTTuMHVQK47fGc5wsmlqekB3nQsTdOXjF9zB14wYCB8g0c

- https://docs.microsoft.com/en-us/visualstudio/modeling/modeling-sdk-for-visual-studio-domain-specific-languages?view=vs-2017

- https://confluence.jetbrains.com/display/MPSD20182/Typesystem
