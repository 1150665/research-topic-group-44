# 0 - Prefácio

Grupo 22 ~ M1A ~ SFA

- <1140865@isepipp.pt> ~ André Silva

- <1140867@isep.ipp.pt> ~ Hélder Diniz

Tópico: **Modeling**

Abordagem: **Modeling without EMF**

# 1 - Descrição do Tópico

### 1.1 - Introdução

Modelação de dados é o processo utilizado para a acção de definir e analisar requisitos de informação, tipicamente associados à representação de processos de negócio, com o intuito de formar um domínio de conhecimento do problema, de forma a fazer com que todas as entidades dessa modelação façam sentido num contexto definido. Este tipo de modelação conceptual tem várias derivações para vertentes equivalentes, tais como: modelo de domínio, modelo de dados e modelo lógico.

Em Engenharia de Domínio (EDOM), o foco principal é a modelação de domínio, assim sendo o foco deste relatório seja adjacente aos conteúdos da unidade curricular.

### 1.2 - Modelação de Domínio

Modelação de domínio descreve a forma de relacionar e descrever entidades reais e os relacionamentos entre elas, que quando se efetua uma contextualização com o sistema, descrevem o problema representado pelo espaço de domínio. Este tipo de modelação é derivado através de conhecimento de requisitos de alto nível do sistema e define uma base efetiva para o assentar o conhecimento de negócio, que auxiliará o *design* do sistema com um objetivo conciso, tal como a manutenibilidade, desenvolvimento contínuo e testabilidade.

Esta abordagem de modelação vem auxiliar o problema que existe na relação do entendimento do conhecimento e o relacionamento desse conhecimento com a interpretação dos requisitos.

Existem diversas formas de representação da modelação de domínio, a mais comum sendo a UML. 

# 2 - Descrição da Abordagem
### 2.1 - EMF

Eclipse Modeling Framework (EMF) é uma *framework* de modelação domínio, com funcionalidade de geração de código, destinado a facilitar a construção de sistemas altamente baseados em regras de negócio complexas. EMF tem uma distinção definida entre modelo e meta-modelo, sendo que o meta-modelo descreve a estrutura do modelo e o modelo é a instanciação concreta desse meta-modelo. Estes meta-modelos podem ser criados de formas diversas, tais como XMI, anotações Java, UML ou até mesmo XML. 

A informação definida nos meta-modelos é utilizada para gerar diversos resultados, sendo que o caso mais típico é utilizar o meta-modelo para gerar uma instância válida, incluíndo o código adjacente, seguindo o modelo de domínio.

### 2.2 - Microsoft Modeling SDK

Tal como o EMF, existem várias *frameworks* para modelação de domínio, com características semelhantes e com objetivos muito parecidos. A tecnologia escolhida para ser investigada neste relatório foi a Microsoft Modeling SDK. 

Esta ferramenta permite, de igual forma, fazer modelação de domínio e tem a funcionalidade de geração de código em linguagem C#. A definição do modelo pode ser feita através de XML ou com a definição de diagrama, com um esquema parecido a UML, ambas as formas são definidas formalmente por uma DSL. Após a definição desse modelo, a *framework* irá gerar os seguintes artefactos:

- Uma implementação do modelo fortemente tipada;
- Um explorador de modelo em forma de árvore;
- Um editor gráfico onde é possível visualizar o modelo definido previamente;
- Métodos de serialização de forma a persistir o modelo em XML;
- Funcionalidade para geração de código e outros variados artefactos.



# 3 - Caso de estudo da abordagem

Para investigar esta *framework* alternativa ao EMF, foi reproduzido o metamodelo de **Requirements** abordado ao longo dos incrementos de EDOM. 

Para criar uma DSL utilizado a Modeling SDK, é necessário ter as DSL Tools instaladas no Visual Studio. Após a instalação, deve-se criar um novo projecto do tipo "Domain-Specific Language Designer" com o nome "RequirementsModel".

Observa-se de imediato a criação de vários ficheiros. O mais importante é o **RequirementsModel.dsl**, onde estão declarados todos os metamodelos em formato XML. Este ficheiro aparenta estar construído de forma bastante mais legível para o developer, quando comparado com o **RequirementsModel.ecore** do EMF. Além de o ficheiro ecore ter mais *bloat* quando comparado com o .dsl (e representarem exactamente o mesmo modelo), repara-se também que as validações em EMF ficam directamente no ficheiro .ecore, com caracteres estranhos misturados com OCL, que provavelmente só serão correctamente interpretados pelo parser do EMF. Assim, de um ponto de vista de edição de texto manual, torna-se quase impossível desenvolver um metamodelo sem o auxílio de um IDE.

![DSL1.PNG](https://bitbucket.org/repo/z84gEKE/images/3434039284-DSL1.PNG)

![DSL2.PNG](https://bitbucket.org/repo/z84gEKE/images/2710302606-DSL2.PNG)

As validações no Modeling SDK são colocadas em extensões das classes geradas,muito mais intuitivas para o programador e não misturadas com XML. Isto será explicado mais à frente.

Para este metamodelo é necessário criar três tipos de dados primitivos, neste caso enumerados: **Priority**, **Type** e **Resolution**. A *framework* obviamente suporta isto e equipara estes três tipos customizados aos da .NET Framework (i.e., String, Int32, Float, etc).

Após a criação dos metamodelos e das suas propriedades (ou campos, em Java), que é em todo muito semelhante ao EMF, torna-se necessário colocar as relações entre si. Ao contrário da criação dos metamodelos, todo este passo é feito utilizando a UI, arrastando o cursor de um metamodelo *source* para um metamodelo *target*. A estas ligações podem ser acrescentadas propriedades visto que, em tempo de transformação, as ligações são mapeadas em classes próprias, o que pode ser útil no caso de existirem certas características associadas a uma ligação.

![Diagrama.PNG](https://bitbucket.org/repo/z84gEKE/images/1699670162-Diagrama.PNG)

Após a criação das ligações, podemos também adicionar validações aos metamodelos ou alterar o modo como se apresentam os modelos na UI (i.e., texto do modelo). No caso das valiações, como dito anteriormente, são feitas extensões aos metamodelos e declarados métodos com anotações específicas, que serão observadas pelo compilador e pelo transformador. Esta abordagem permite ter quer uma maior distribuição de responsabilidades entre ficheiros (não fica tudo num .ecore), quer ter uma maior facilidade no desenvolvimento nas validações, visto que são declaradas em código C# e não noutra linguagem menos utilizada (e diferente) como a OCL.

Um exemplo de uma validação pode ser encontrada abaixo.

![ValidationExample.PNG](https://bitbucket.org/repo/z84gEKE/images/1310528513-ValidationExample.PNG)

Estas validações podem ser chamadas em vários momentos "de vida" da instância do metamodelo (e.g., carregamento do ficheiro, alteração de uma propriedade, etc).

Após tudo isto, podemos então executar uma nova instância do Visual Studio onde estas alterações são visiveis.

![Instance.PNG](https://bitbucket.org/repo/z84gEKE/images/422852268-Instance.PNG)


# 4 - Conclusão

Considerando todos os factos enunciados nos pontos anteriores, é possível verificar que existem poucas diferenças entre EMF e Microsoft SDK, sendo que a maior distinção encontra-se no ecossistema em que as tecnologias se encontram e o *lifecycle* de validações. Na seguinte lista podemos ver as similaridades das duas tecnologias:

- Definição de meta-modelos através de XML ou diagramas;
- Criação de novas instâncias de modelo, utilizando o meta-modelo base;
- Geração de código (Java no EMF, C# no Microsoft SDK).

No entanto, existe uma diferença muito notória entre estas duas tecnologias, nomeadamente no *lifecyle* das validações. No EMF, as validações podem ser feitas aquando o desenvolvimento do meta-modelo, enquanto que no Microsoft SDK, as validações são desenvolvidas após a geração de código definido pelo meta-modelo.

Também a estrutura do XML do metamodelo é bastante mais legível no Modeling SDK, permitindo até a edição manual.

Assim sendo, é possível concluir que Microsoft Modeling SDK é uma ferramenta bastante capaz e com várias funcionalidades para modelação de domínio e validações, ao nível do EMF, algo que poderá ser de possível interesse para futuras edições de EDOM.

# 5 - Bibliografia

- P of EAA: Domain Model - Martin Fowler, 2003
- [Modeling SDK for Visual Studio - Domain-Specific Languages](https://docs.microsoft.com/en-us/visualstudio/modeling/modeling-sdk-for-visual-studio-domain-specific-languages?view=vs-2017) - Microsoft, 2016
- Eclipse Modeling Framework: A Developer's Guide - Frank Budinsky, David Steinberg, Raymond Ellersick, Timothy J. Grose, Ed Merks, 2016