﻿# Model Validation using GPL

## Validação de Modelos
Um modelo é uma coleção de objetos e relações entre objetos que, juntos, fornecem uma representação real de um sistema. 
A validação de modelos é crucial no processo de desenvolvimento, e deve ser realizada o mais cedo possível.
Esta validação pode incidir sobre duas vertentes: validação da semântica ou avaliação dos critérios de qualidade.
Para validar os modelos é possível optar pela utilização de uma DSL (Domain Specific Language) ou de uma GPL (General Purpose Language).
Uma DSL pode ser definida como uma linguagem de programação que utiliza notação especifica para um problema de domínio em particular. 
A ferramenta mais utilizada para validar o modelo representado pela DSL é o OCL (Object Constraint Language).

## Validação de Modelos usando OCL
O OCL é uma linguagem declarativa que permite definir o comportamento da estrutura de um metamodelo ou permite complementar esse metamodelo. 
O OCL compreende o modelo, utilizando uma sintaxe próxima do metamodelo.

## GPL

### O que é?
General-purpose Programming Language é uma linguagem de programação utilizada para validação de uma variedade de modelos.
Ao contrário de OCL, GPL é uma linguagem que não implementa restrições específicas a um determinado modelo, mas sim, as restrições que são transversais aos vários modelos.

Como exemplos de linguagens GPL, podem-se considerar as seguintes:

* C;
* C++;
* PHP;
* Scala;
* JAVA;
* JavaScript.

### Implementação da validação de modelos
Utilizando GPL para validação de modelos, foram implementadas validações mais abstratas, ou seja, validações genéricas ao diferentes modelos. Tais como:

* Validação de formatos de datas, validação de números.

A título de exemplo, foi desenvolvida uma aplicação que valida modelos de "Requirements", abordados nas aulas de EDOM. 
O programa começa por ler e validar a formatação do ficheiro .xmi, sem prestar atenção ao "conteúdo". 
Desta forma, o ficheiro é validado quanto à sua estrutura (por exemplo, falta de *tags* ou datas que não possam ser lidas). 
Depois disto, é executada a validação do modelo em si. Neste caso, um método *execute()* que começa pelo *root node* (Model), validando que o título não está vazio e, de seguida, valida todos os *RequirementGroups*. 
Este processo repete-se para os *Requirements* e para os *Comments*. Se nenhuma excepção foi lançada, quer dizer que o modelo se encontra corretamente estruturado e com valores corretos.

De seguida, irão ser mostrados os excertos de código responsáveis pelas validações descritas anteriormente.

#### Validação da estrutura do modelo
A aplicação utiliza os métodos utilizados nas aulas para carregar para memória um ficheiro .xmi. Desta forma, se este processo se completar sem nenhuma excepção lançada, existe a garantia de que o modelo 
está, no mínimo, bem formatado.

```

#!java
public XMIParser(String file) throws Exception {
		try {
			Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl());

			ProjectPackage.eINSTANCE.eClass();

			ProjectFactory factory = ProjectFactory.eINSTANCE;

			ResourceSet resSet = new ResourceSetImpl();

			Resource resource = resSet.getResource(URI.createURI(file), true);

			resource.load(Collections.EMPTY_MAP);

			EObject root = resource.getContents().get(0);

			this.model = (Model) root;
		} catch (Exception e) {
			throw new Exception("Input file cannot be parsed.");
		}
	}
```

#### Validação do conteúdo do modelo
Depois de carregado para memória, começa a validação do contéudo do ficheiro .xmi. Primeiro, começa-se por validar o título do modelo e os seus *child nodes*. Neste caso, o modelo "Requirements" apenas tem
como filhos um conjunto de "RequirementsGroup".

```

#!java
	public void execute() throws Exception {
		validateModel();
	}

	private void validateModel() throws Exception {
		// Validate Title
		this.messagePrefix = "Model Title";
		validateString(model.getTitle());

		for (RequirementGroup reqGroup : this.model.getGroups()) {
			validateRequirementGroup(reqGroup);
		}

		System.out.println("\nAll requirements groups were valid.");
	}
```

Mais uma vez, é aplicada o mesmo tipo de lógica. Primeiro valida-se os atributos do pai e depois são iterados os filhos e faz-se a validação individual de cada um. No caso dos "RequirementsGroup", é feita 
a validação do ID (não pode ser vazio e, caso seja um número, não pode ser negativo) e da descrição.

```

#!java
	private void validateRequirementGroup(RequirementGroup reqGroup) throws Exception {
		this.messagePrefix = "Requirement Group \"" + reqGroup.getName() + "\"";

		// Validate ID
		validateID(reqGroup.getId());

		this.messagePrefix += "Description";
		// Validate Description
		validateString(reqGroup.getDescription());

		// Validate Requirements
		for (Requirement req : reqGroup.getRequirements()) {
			validateRequirement(req);
		}

		System.out.println("\nRequirement Group \"" + reqGroup.getName() + "\" requirements were valid.");
	}
```

Tal como os "RequirementsGroup", é feita o mesmo tipo de validação. A única diferença é a adição da validação das datas. Neste caso, é definido que a data de criação de um requirement é sempre anterior à 
data de criação de um dos seus comentários.

```
#!java

private void validateRequirement(Requirement req) throws Exception {
		// Validate Title
		this.messagePrefix = "Requirement Title";
		validateString(req.getTitle());

		String prefix = "Requirement \"" + req.getTitle() + "\"";
		this.messagePrefix = prefix;

		// Validate ID
		validateID(req.getId());

		// Validate Description
		this.messagePrefix = prefix + " Description";
		validateString(req.getDescription());

		// Validate Author
		this.messagePrefix = prefix + " Author";
		validateString(req.getAuthor());

		// Validate the Version
		this.messagePrefix = prefix + " Version";
		validateVersion(req.getVersion());

		// For comment validation
		Date requirementDate = req.getCreated();

		// Validate comments
		for (Comment comment : req.getComments()) {
			this.messagePrefix = prefix + " Comment";
			validateComment(comment);

			// Validates Date
			Date commentDate = comment.getCreated();

			if (requirementDate.after(commentDate)) {
				throw new Exception(this.messagePrefix + " Date is not correct");
			}
		}
	}
```


O caso dos "Comments" é algo mais complexo devido à possibilidade de este ter filhos "Comments" que por sua vez podem também ter filhos "Comments.". Portanto, é primeiro feita uma validação aos atributos do
"Comment" e, por fim, é feita uma validação recursiva aos seus filhos.

```

#!java

	private void validateComment(Comment comment) throws Exception {
		String prefix = this.messagePrefix;
		this.messagePrefix = prefix;
		
		// Validate Author
		this.messagePrefix = prefix + " Author";
		validateString(comment.getAuthor());
		
		// Validate Author
		this.messagePrefix = prefix + " Subject";
		validateString(comment.getSubject());
		
		// Validate Author
		this.messagePrefix= prefix + " Body";
		validateString(comment.getBody());
		
		// Recursively validate children
		for(Comment child : comment.getChildren()) {
			validateComment(child);
		}
	}
```

Os seguintes métodos são os métodos de "ajuda" à validação que foram chamados nos excertos anteriores. O "validateString" apenas verifica se a String está vazia e o "validateID", além dessa mesma validação,
verifica também se essa String pode ser convertida para um número e, caso possa, verifica que é um número positivo.

```
#!java

	private void validateID(String id) throws Exception {
		this.messagePrefix += " ID";
		validateString(id);
		
		// If ID is a number, it cannot be negative
		long idAsNumber = 0;

		try {
			idAsNumber = Long.parseLong(id);
		} catch (Exception e) {

		}

		if (idAsNumber < 0) {
			throw new Exception(this.messagePrefix + " cannot be a negative number.");
		}

	}

	private void validateString(String string) throws Exception {
		if (string == null || string.trim().isEmpty()) {
			throw new Exception(this.messagePrefix + " cannot be empty.");
		}
	}
```

## OCL vs GPL 

Analisando a implementação de uma solução que valide um modelo adotando GPL, concluiu-se que esta abordagem tem poucas vantagens alternativamente a uma validação adotando OCL. 
A adoção de GPL requer o desenvolvimento de uma aplicação externa, pelo que torna a validação menos fiável e com uma menor usabilidade. Esta abordagem também não permite a utilização de “live validation”, ou seja, não é possível validar um determinado modelo à medida que este vai sendo definido no IDE.
Na abordagem realizada, a maior vantagem prende-se ao facto de existir uma maior diversidade de linguagens que podem ser utilizadas para implementação das validações (Java, C++, C#, etc), sendo que, o developer consegue facilmente escolher aquela com a qual tem um maior à vontade.

Relativamente à fiabilidade indicada anteriormente, esta refere-se à possibilidade de existirem erros provenientes da dependência de um _parser_ do modelo. Esta situação pode fazer com que se esteja a proceder a uma validação diferente daquela que realmente foi definida.
Por outro lado, no caso da utilização de GPL para validar especificidades do modelo, sempre que este seja alterado, teremos que alterar também a validação definida para que seja possível contemplar todas as novas alterações. 
Portanto, isto faz com que se tenham que alterar sempre ambas as partes, tornando este processo menos dinâmico e mais árduo.

###Sugestão de solução 

Como sugestão, uma possível solução poderia combinar o melhor das duas validações, fazendo uma primeira validação utilizando GPL seguida de uma validação utilizando OCL.
Esta solução permite a possibilidade de realização de uma validação inicial que seja mais genérica e transversal a diferentes modelos, enquanto que a segunda validação já iria incidir sobre as especificidades do modelo.
Pode-se olhar para o seguinte cenário:

	Uma determinada empresa incide sobre dois negócios distintos: Imobiliário e eCommerce.
	Estes negócios têm modelos distintos com especificidades diferentes, mas também têm regras e restrições em comum. 

Analisando o cenário anterior, seria demasiado custoso, além de que seria uma má prática, a repetição de restrições em ambos os modelos. 
Desta forma, o ideal passaria pela adoção de uma solução de validação "híbrida", em que, as restrições mais abstratas e comuns aos modelos seriam implementadas através de GPL, enquanto que as específicas de cada modelo seriam implementadas através de OCL. 
Esta solução permite que sejam adotadas boas práticas, tais como _Don't Repeat Yourself_ (DRY) e simultaneamente utiliza as melhores características de ambos: Abstração das validação de GPL e a especificidade das validações de OCL.

##Referências

* Queralt, A. & Teniente, E. (2012). Verification and Validation of UML Conceptual Schemas with OCL Constraints. _ACM Transactions on Software Engineering and Methodology_, _21_(2), 1-41.
* Mernik, M., Heering, J. & Sloane, A. (2005). When and how to develop domain-specific languages. _ACM Computing Surveys_, _37_(4), 316-344.
* Damus, C., Sánchez-Barbudo, A., Herrera, U. A. & Willink, E. (2002). OCL Documentation.