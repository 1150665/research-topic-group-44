# EDOM - Project Research Topic
# Modeling with other frameworks (Microsoft DSL SDK)

## Descrição
O **Microsoft DSL SDK** (MSDK) da Microsoft permite desenvolver, através de modelos e de forma integrada com o Visual Studio.

## Abordagem
Este trabalho teórico tem como objetivo a comparação do MSDK com as tecnologias usadas ao longo da unidade curricular. 
Não será feita uma comparação no sentido de evidênciar vantagens e desvantagens mas sim de aprentação de uma outra abordagem e tecnologia.  

## Caso de estudo 
Para fazer a comparação entre as duas tencnologias, será usado o modelo dos **Requirements**. A comparação terá as seguintes etapas:

1. Criar o modelo
2. Fazer validações ao modelo
3. Criar uma gramática
4. Tranformação do modelo / Edição de modelos por código
5. Geração de código
5. Utilização da DSL

## Comparação das abordagens

### **Criar o modelo**

A criação do modelo é muito semelhante à solução lecionada. É feita através de Domain Classes, Domain Propeties e relações entre as classes que no caso do MSDK podem ser do tipo Embedding ou Reference.

![alt text](Model.jpg "Modelo")

Os modelos criados em MSDK têm também uma representação em notação XML que pode ser editada manualmente.

![alt text](ModelXML.jpg "Modelo")

A Microsoft acrescentou ainda um outro conceito aos modelos que permite especificar a interface gráfica do modelo quando é editado/construído no Visual Studio. Essa representação, que tembém fica definida no próprio modelo é construída atrvés de Geometry Shapes que presentam as classes e Connectors que representam as ligações entre as classes.
É possível customizar a forma como os objetos são desenhado fazendo a ligação entre as Domain Properties e as properiedades do objeto. 

![alt text](ModelSHAPE.jpg "Modelo")

A Shapes permitem fazer a representação gráfica do modelo de forma a ser mais simples a utilização pelos seus stakeholders. A imagem seguinte demonstra uma representação gráfica do metamodelo Requirements.

![alt text](ModelEDIT.jpg "Modelo")

À semelhança do EMF, os modelos também ficam guardados num ficheiro XML com referência ao METAMODELO como demonstra a imagem seguinte.

![alt text](ModelEDITXML.jpg "Modelo")

### **Fazer validações ao modelo**
As validações são feitas através de extensibilidade às própricas classes do metamodelo recorrendo a partial classes e anotações para escrever as valiações em C# ou VB.NET dependnedo da linguagem do projeto. 

A imagem seguinte representa um exemplo de uma validação

![alt text](ModelValidation.jpg "Modelo")

### **Criar uma gramática**
O MSDK não tem suporte "nativo" para gramáticas (XText). Tem apenas algumas formas de customizar a serialização do modelo.

### **Tranformação do modelo / Edição de modelos por código**
O MSDK não tem nenhuma ferramenta do género do ATL mas permite fazer a manupulação de modelos por código. 

![alt text](ModelCODE.jpg "Modelo")

### **Geração de código**
O MSDK permite gerar código com recurso "T4 text templates". Mais uma vez o C# está presente e transformação é uma mistura de código C# que percorre o modelo ligado e gera texto. 

![alt text](TT1.jpg "Modelo")

1. Template de transformação

![alt text](TT2.jpg "Modelo")

2. Ficheiro gerado em tempo real

![alt text](TT3.jpg "Modelo")

3. Resultado da geração



### **Utilização da DSL**
A forma de utilizar a DSL criada é atevés de uma extensão para o Visual Studio, que com a instalação da DSL passa a reconhecer ficheiros do tipo da DSL. 

![alt text](VSVSIX.jpg "Modelo")

1. Ficheiro da extensão

![alt text](VSVSIX2.jpg "Modelo")

2. Instalação da extensão

![alt text](VSVSIX3.jpg "Modelo")

3. Criação de um ficheiro de requirements

![alt text](VSVSIX4.jpg "Modelo")

3. Edição de um ficheiro de requirements


## Referências
https://docs.microsoft.com/en-us/visualstudio/modeling/modeling-sdk-for-visual-studio-domain-specific-languages?view=vs-2017

