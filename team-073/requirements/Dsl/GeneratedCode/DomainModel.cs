﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using DslModeling = global::Microsoft.VisualStudio.Modeling;
using DslDesign = global::Microsoft.VisualStudio.Modeling.Design;
using DslDiagrams = global::Microsoft.VisualStudio.Modeling.Diagrams;
namespace edom.requirements
{
	/// <summary>
	/// DomainModel requirementsDomainModel
	/// Description for edom.requirements.requirements
	/// </summary>
	[DslDesign::DisplayNameResource("edom.requirements.requirementsDomainModel.DisplayName", typeof(global::edom.requirements.requirementsDomainModel), "edom.requirements.GeneratedCode.DomainModelResx")]
	[DslDesign::DescriptionResource("edom.requirements.requirementsDomainModel.Description", typeof(global::edom.requirements.requirementsDomainModel), "edom.requirements.GeneratedCode.DomainModelResx")]
	[global::System.CLSCompliant(true)]
	[DslModeling::DependsOnDomainModel(typeof(global::Microsoft.VisualStudio.Modeling.CoreDomainModel))]
	[DslModeling::DependsOnDomainModel(typeof(global::Microsoft.VisualStudio.Modeling.Diagrams.CoreDesignSurfaceDomainModel))]
	[global::System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling", Justification = "Generated code.")]
	[DslModeling::DomainObjectId("0d07933c-7007-40fe-b015-1fb99ac4a85a")]
	public partial class requirementsDomainModel : DslModeling::DomainModel
	{
		#region Constructor, domain model Id
	
		/// <summary>
		/// requirementsDomainModel domain model Id.
		/// </summary>
		public static readonly global::System.Guid DomainModelId = new global::System.Guid(0x0d07933c, 0x7007, 0x40fe, 0xb0, 0x15, 0x1f, 0xb9, 0x9a, 0xc4, 0xa8, 0x5a);
	
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="store">Store containing the domain model.</param>
		public requirementsDomainModel(DslModeling::Store store)
			: base(store, DomainModelId)
		{
			// Call the partial method to allow any required serialization setup to be done.
			this.InitializeSerialization(store);		
		}
		
	
		///<Summary>
		/// Defines a partial method that will be called from the constructor to
		/// allow any necessary serialization setup to be done.
		///</Summary>
		///<remarks>
		/// For a DSL created with the DSL Designer wizard, an implementation of this 
		/// method will be generated in the GeneratedCode\SerializationHelper.cs class.
		///</remarks>
		partial void InitializeSerialization(DslModeling::Store store);
	
	
		#endregion
		#region Domain model reflection
			
		/// <summary>
		/// Gets the list of generated domain model types (classes, rules, relationships).
		/// </summary>
		/// <returns>List of types.</returns>
		[global::System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling", Justification = "Generated code.")]	
		protected sealed override global::System.Type[] GetGeneratedDomainModelTypes()
		{
			return new global::System.Type[]
			{
				typeof(Model),
				typeof(RequirementGroup),
				typeof(Requirement),
				typeof(Comment),
				typeof(Version),
				typeof(ModelHasGroups),
				typeof(RequirementGroupHasRequirements),
				typeof(RequirementHasComments),
				typeof(RequirementHasVersion),
				typeof(RequirementsDiagram),
				typeof(Connector),
				typeof(RequirementGroupShape),
				typeof(RequirementShape),
				typeof(CommentShape),
				typeof(VersionShape),
				typeof(global::edom.requirements.FixUpDiagram),
				typeof(global::edom.requirements.ConnectorRolePlayerChanged),
			};
		}
		/// <summary>
		/// Gets the list of generated domain properties.
		/// </summary>
		/// <returns>List of property data.</returns>
		[global::System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling", Justification = "Generated code.")]	
		protected sealed override DomainMemberInfo[] GetGeneratedDomainProperties()
		{
			return new DomainMemberInfo[]
			{
				new DomainMemberInfo(typeof(Model), "title", Model.titleDomainPropertyId, typeof(Model.titlePropertyHandler)),
				new DomainMemberInfo(typeof(RequirementGroup), "name", RequirementGroup.nameDomainPropertyId, typeof(RequirementGroup.namePropertyHandler)),
				new DomainMemberInfo(typeof(RequirementGroup), "description", RequirementGroup.descriptionDomainPropertyId, typeof(RequirementGroup.descriptionPropertyHandler)),
				new DomainMemberInfo(typeof(RequirementGroup), "id", RequirementGroup.idDomainPropertyId, typeof(RequirementGroup.idPropertyHandler)),
				new DomainMemberInfo(typeof(Requirement), "title", Requirement.titleDomainPropertyId, typeof(Requirement.titlePropertyHandler)),
				new DomainMemberInfo(typeof(Requirement), "description", Requirement.descriptionDomainPropertyId, typeof(Requirement.descriptionPropertyHandler)),
				new DomainMemberInfo(typeof(Requirement), "id", Requirement.idDomainPropertyId, typeof(Requirement.idPropertyHandler)),
				new DomainMemberInfo(typeof(Comment), "subject", Comment.subjectDomainPropertyId, typeof(Comment.subjectPropertyHandler)),
				new DomainMemberInfo(typeof(Comment), "body", Comment.bodyDomainPropertyId, typeof(Comment.bodyPropertyHandler)),
				new DomainMemberInfo(typeof(Comment), "author", Comment.authorDomainPropertyId, typeof(Comment.authorPropertyHandler)),
				new DomainMemberInfo(typeof(Comment), "created", Comment.createdDomainPropertyId, typeof(Comment.createdPropertyHandler)),
				new DomainMemberInfo(typeof(Version), "major", Version.majorDomainPropertyId, typeof(Version.majorPropertyHandler)),
				new DomainMemberInfo(typeof(Version), "minor", Version.minorDomainPropertyId, typeof(Version.minorPropertyHandler)),
				new DomainMemberInfo(typeof(Version), "service", Version.serviceDomainPropertyId, typeof(Version.servicePropertyHandler)),
			};
		}
		/// <summary>
		/// Gets the list of generated domain roles.
		/// </summary>
		/// <returns>List of role data.</returns>
		protected sealed override DomainRolePlayerInfo[] GetGeneratedDomainRoles()
		{
			return new DomainRolePlayerInfo[]
			{
				new DomainRolePlayerInfo(typeof(ModelHasGroups), "Model", ModelHasGroups.ModelDomainRoleId),
				new DomainRolePlayerInfo(typeof(ModelHasGroups), "Element", ModelHasGroups.ElementDomainRoleId),
				new DomainRolePlayerInfo(typeof(RequirementGroupHasRequirements), "RequirementGroup", RequirementGroupHasRequirements.RequirementGroupDomainRoleId),
				new DomainRolePlayerInfo(typeof(RequirementGroupHasRequirements), "Requirement", RequirementGroupHasRequirements.RequirementDomainRoleId),
				new DomainRolePlayerInfo(typeof(RequirementHasComments), "Requirement", RequirementHasComments.RequirementDomainRoleId),
				new DomainRolePlayerInfo(typeof(RequirementHasComments), "Comment", RequirementHasComments.CommentDomainRoleId),
				new DomainRolePlayerInfo(typeof(RequirementHasVersion), "Requirement", RequirementHasVersion.RequirementDomainRoleId),
				new DomainRolePlayerInfo(typeof(RequirementHasVersion), "Version", RequirementHasVersion.VersionDomainRoleId),
			};
		}
		#endregion
		#region Factory methods
		private static global::System.Collections.Generic.Dictionary<global::System.Type, int> createElementMap;
	
		/// <summary>
		/// Creates an element of specified type.
		/// </summary>
		/// <param name="partition">Partition where element is to be created.</param>
		/// <param name="elementType">Element type which belongs to this domain model.</param>
		/// <param name="propertyAssignments">New element property assignments.</param>
		/// <returns>Created element.</returns>
		[global::System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
		[global::System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling", Justification = "Generated code.")]	
		public sealed override DslModeling::ModelElement CreateElement(DslModeling::Partition partition, global::System.Type elementType, DslModeling::PropertyAssignment[] propertyAssignments)
		{
			if (elementType == null) throw new global::System.ArgumentNullException("elementType");
	
			if (createElementMap == null)
			{
				createElementMap = new global::System.Collections.Generic.Dictionary<global::System.Type, int>(11);
				createElementMap.Add(typeof(Model), 0);
				createElementMap.Add(typeof(RequirementGroup), 1);
				createElementMap.Add(typeof(Requirement), 2);
				createElementMap.Add(typeof(Comment), 3);
				createElementMap.Add(typeof(Version), 4);
				createElementMap.Add(typeof(RequirementsDiagram), 5);
				createElementMap.Add(typeof(Connector), 6);
				createElementMap.Add(typeof(RequirementGroupShape), 7);
				createElementMap.Add(typeof(RequirementShape), 8);
				createElementMap.Add(typeof(CommentShape), 9);
				createElementMap.Add(typeof(VersionShape), 10);
			}
			int index;
			if (!createElementMap.TryGetValue(elementType, out index))
			{
				// construct exception error message		
				string exceptionError = string.Format(
								global::System.Globalization.CultureInfo.CurrentCulture,
								global::edom.requirements.requirementsDomainModel.SingletonResourceManager.GetString("UnrecognizedElementType"),
								elementType.Name);
				throw new global::System.ArgumentException(exceptionError, "elementType");
			}
			switch (index)
			{
				case 0: return new Model(partition, propertyAssignments);
				case 1: return new RequirementGroup(partition, propertyAssignments);
				case 2: return new Requirement(partition, propertyAssignments);
				case 3: return new Comment(partition, propertyAssignments);
				case 4: return new Version(partition, propertyAssignments);
				case 5: return new RequirementsDiagram(partition, propertyAssignments);
				case 6: return new Connector(partition, propertyAssignments);
				case 7: return new RequirementGroupShape(partition, propertyAssignments);
				case 8: return new RequirementShape(partition, propertyAssignments);
				case 9: return new CommentShape(partition, propertyAssignments);
				case 10: return new VersionShape(partition, propertyAssignments);
				default: return null;
			}
		}
	
		private static global::System.Collections.Generic.Dictionary<global::System.Type, int> createElementLinkMap;
	
		/// <summary>
		/// Creates an element link of specified type.
		/// </summary>
		/// <param name="partition">Partition where element is to be created.</param>
		/// <param name="elementLinkType">Element link type which belongs to this domain model.</param>
		/// <param name="roleAssignments">List of relationship role assignments for the new link.</param>
		/// <param name="propertyAssignments">New element property assignments.</param>
		/// <returns>Created element link.</returns>
		[global::System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
		public sealed override DslModeling::ElementLink CreateElementLink(DslModeling::Partition partition, global::System.Type elementLinkType, DslModeling::RoleAssignment[] roleAssignments, DslModeling::PropertyAssignment[] propertyAssignments)
		{
			if (elementLinkType == null) throw new global::System.ArgumentNullException("elementLinkType");
			if (roleAssignments == null) throw new global::System.ArgumentNullException("roleAssignments");
	
			if (createElementLinkMap == null)
			{
				createElementLinkMap = new global::System.Collections.Generic.Dictionary<global::System.Type, int>(4);
				createElementLinkMap.Add(typeof(ModelHasGroups), 0);
				createElementLinkMap.Add(typeof(RequirementGroupHasRequirements), 1);
				createElementLinkMap.Add(typeof(RequirementHasComments), 2);
				createElementLinkMap.Add(typeof(RequirementHasVersion), 3);
			}
			int index;
			if (!createElementLinkMap.TryGetValue(elementLinkType, out index))
			{
				// construct exception error message
				string exceptionError = string.Format(
								global::System.Globalization.CultureInfo.CurrentCulture,
								global::edom.requirements.requirementsDomainModel.SingletonResourceManager.GetString("UnrecognizedElementLinkType"),
								elementLinkType.Name);
				throw new global::System.ArgumentException(exceptionError, "elementLinkType");
			
			}
			switch (index)
			{
				case 0: return new ModelHasGroups(partition, roleAssignments, propertyAssignments);
				case 1: return new RequirementGroupHasRequirements(partition, roleAssignments, propertyAssignments);
				case 2: return new RequirementHasComments(partition, roleAssignments, propertyAssignments);
				case 3: return new RequirementHasVersion(partition, roleAssignments, propertyAssignments);
				default: return null;
			}
		}
		#endregion
		#region Resource manager
		
		private static global::System.Resources.ResourceManager resourceManager;
		
		/// <summary>
		/// The base name of this model's resources.
		/// </summary>
		public const string ResourceBaseName = "edom.requirements.GeneratedCode.DomainModelResx";
		
		/// <summary>
		/// Gets the DomainModel's ResourceManager. If the ResourceManager does not already exist, then it is created.
		/// </summary>
		public override global::System.Resources.ResourceManager ResourceManager
		{
			[global::System.Diagnostics.DebuggerStepThrough]
			get
			{
				return requirementsDomainModel.SingletonResourceManager;
			}
		}
	
		/// <summary>
		/// Gets the Singleton ResourceManager for this domain model.
		/// </summary>
		public static global::System.Resources.ResourceManager SingletonResourceManager
		{
			[global::System.Diagnostics.DebuggerStepThrough]
			get
			{
				if (requirementsDomainModel.resourceManager == null)
				{
					requirementsDomainModel.resourceManager = new global::System.Resources.ResourceManager(ResourceBaseName, typeof(requirementsDomainModel).Assembly);
				}
				return requirementsDomainModel.resourceManager;
			}
		}
		#endregion
		#region Copy/Remove closures
		/// <summary>
		/// CopyClosure cache
		/// </summary>
		private static DslModeling::IElementVisitorFilter copyClosure;
		/// <summary>
		/// DeleteClosure cache
		/// </summary>
		private static DslModeling::IElementVisitorFilter removeClosure;
		/// <summary>
		/// Returns an IElementVisitorFilter that corresponds to the ClosureType.
		/// </summary>
		/// <param name="type">closure type</param>
		/// <param name="rootElements">collection of root elements</param>
		/// <returns>IElementVisitorFilter or null</returns>
		public override DslModeling::IElementVisitorFilter GetClosureFilter(DslModeling::ClosureType type, global::System.Collections.Generic.ICollection<DslModeling::ModelElement> rootElements)
		{
			switch (type)
			{
				case DslModeling::ClosureType.CopyClosure:
					return requirementsDomainModel.CopyClosure;
				case DslModeling::ClosureType.DeleteClosure:
					return requirementsDomainModel.DeleteClosure;
			}
			return base.GetClosureFilter(type, rootElements);
		}
		/// <summary>
		/// CopyClosure cache
		/// </summary>
		private static DslModeling::IElementVisitorFilter CopyClosure
		{
			get
			{
				// Incorporate all of the closures from the models we extend
				if (requirementsDomainModel.copyClosure == null)
				{
					DslModeling::ChainingElementVisitorFilter copyFilter = new DslModeling::ChainingElementVisitorFilter();
					copyFilter.AddFilter(new requirementsCopyClosure());
					copyFilter.AddFilter(new DslModeling::CoreCopyClosure());
					copyFilter.AddFilter(new DslDiagrams::CoreDesignSurfaceCopyClosure());
					
					requirementsDomainModel.copyClosure = copyFilter;
				}
				return requirementsDomainModel.copyClosure;
			}
		}
		/// <summary>
		/// DeleteClosure cache
		/// </summary>
		private static DslModeling::IElementVisitorFilter DeleteClosure
		{
			get
			{
				// Incorporate all of the closures from the models we extend
				if (requirementsDomainModel.removeClosure == null)
				{
					DslModeling::ChainingElementVisitorFilter removeFilter = new DslModeling::ChainingElementVisitorFilter();
					removeFilter.AddFilter(new requirementsDeleteClosure());
					removeFilter.AddFilter(new DslModeling::CoreDeleteClosure());
					removeFilter.AddFilter(new DslDiagrams::CoreDesignSurfaceDeleteClosure());
		
					requirementsDomainModel.removeClosure = removeFilter;
				}
				return requirementsDomainModel.removeClosure;
			}
		}
		#endregion
		#region Diagram rule helpers
		/// <summary>
		/// Enables rules in this domain model related to diagram fixup for the given store.
		/// If diagram data will be loaded into the store, this method should be called first to ensure
		/// that the diagram behaves properly.
		/// </summary>
		public static void EnableDiagramRules(DslModeling::Store store)
		{
			if(store == null) throw new global::System.ArgumentNullException("store");
			
			DslModeling::RuleManager ruleManager = store.RuleManager;
			ruleManager.EnableRule(typeof(global::edom.requirements.FixUpDiagram));
			ruleManager.EnableRule(typeof(global::edom.requirements.ConnectorRolePlayerChanged));
		}
		
		/// <summary>
		/// Disables rules in this domain model related to diagram fixup for the given store.
		/// </summary>
		public static void DisableDiagramRules(DslModeling::Store store)
		{
			if(store == null) throw new global::System.ArgumentNullException("store");
			
			DslModeling::RuleManager ruleManager = store.RuleManager;
			ruleManager.DisableRule(typeof(global::edom.requirements.FixUpDiagram));
			ruleManager.DisableRule(typeof(global::edom.requirements.ConnectorRolePlayerChanged));
		}
		#endregion
	}
		
	#region Copy/Remove closure classes
	/// <summary>
	/// Remove closure visitor filter
	/// </summary>
	[global::System.CLSCompliant(true)]
	public partial class requirementsDeleteClosure : requirementsDeleteClosureBase, DslModeling::IElementVisitorFilter
	{
		/// <summary>
		/// Constructor
		/// </summary>
		public requirementsDeleteClosure() : base()
		{
		}
	}
	
	/// <summary>
	/// Base class for remove closure visitor filter
	/// </summary>
	[global::System.CLSCompliant(true)]
	public partial class requirementsDeleteClosureBase : DslModeling::IElementVisitorFilter
	{
		/// <summary>
		/// DomainRoles
		/// </summary>
		private global::System.Collections.Specialized.HybridDictionary domainRoles;
		/// <summary>
		/// Constructor
		/// </summary>
		public requirementsDeleteClosureBase()
		{
			#region Initialize DomainData Table
			DomainRoles.Add(global::edom.requirements.ModelHasGroups.ElementDomainRoleId, true);
			DomainRoles.Add(global::edom.requirements.RequirementGroupHasRequirements.RequirementDomainRoleId, true);
			DomainRoles.Add(global::edom.requirements.RequirementHasComments.CommentDomainRoleId, true);
			DomainRoles.Add(global::edom.requirements.RequirementHasVersion.VersionDomainRoleId, true);
			#endregion
		}
		/// <summary>
		/// Called to ask the filter if a particular relationship from a source element should be included in the traversal
		/// </summary>
		/// <param name="walker">ElementWalker that is traversing the model</param>
		/// <param name="sourceElement">Model Element playing the source role</param>
		/// <param name="sourceRoleInfo">DomainRoleInfo of the role that the source element is playing in the relationship</param>
		/// <param name="domainRelationshipInfo">DomainRelationshipInfo for the ElementLink in question</param>
		/// <param name="targetRelationship">Relationship in question</param>
		/// <returns>Yes if the relationship should be traversed</returns>
		public virtual DslModeling::VisitorFilterResult ShouldVisitRelationship(DslModeling::ElementWalker walker, DslModeling::ModelElement sourceElement, DslModeling::DomainRoleInfo sourceRoleInfo, DslModeling::DomainRelationshipInfo domainRelationshipInfo, DslModeling::ElementLink targetRelationship)
		{
			return DslModeling::VisitorFilterResult.Yes;
		}
		/// <summary>
		/// Called to ask the filter if a particular role player should be Visited during traversal
		/// </summary>
		/// <param name="walker">ElementWalker that is traversing the model</param>
		/// <param name="sourceElement">Model Element playing the source role</param>
		/// <param name="elementLink">Element Link that forms the relationship to the role player in question</param>
		/// <param name="targetDomainRole">DomainRoleInfo of the target role</param>
		/// <param name="targetRolePlayer">Model Element that plays the target role in the relationship</param>
		/// <returns></returns>
		public virtual DslModeling::VisitorFilterResult ShouldVisitRolePlayer(DslModeling::ElementWalker walker, DslModeling::ModelElement sourceElement, DslModeling::ElementLink elementLink, DslModeling::DomainRoleInfo targetDomainRole, DslModeling::ModelElement targetRolePlayer)
		{
			if (targetDomainRole == null) throw new global::System.ArgumentNullException("targetDomainRole");
			return this.DomainRoles.Contains(targetDomainRole.Id) ? DslModeling::VisitorFilterResult.Yes : DslModeling::VisitorFilterResult.DoNotCare;
		}
		/// <summary>
		/// DomainRoles
		/// </summary>
		private global::System.Collections.Specialized.HybridDictionary DomainRoles
		{
			get
			{
				if (this.domainRoles == null)
				{
					this.domainRoles = new global::System.Collections.Specialized.HybridDictionary();
				}
				return this.domainRoles;
			}
		}
	
	}
	/// <summary>
	/// Copy closure visitor filter
	/// </summary>
	[global::System.CLSCompliant(true)]
	public partial class requirementsCopyClosure : requirementsCopyClosureBase, DslModeling::IElementVisitorFilter
	{
		/// <summary>
		/// Constructor
		/// </summary>
		public requirementsCopyClosure() : base()
		{
		}
	}
	/// <summary>
	/// Base class for copy closure visitor filter
	/// </summary>
	[global::System.CLSCompliant(true)]
	public partial class requirementsCopyClosureBase : DslModeling::CopyClosureFilter, DslModeling::IElementVisitorFilter
	{
		/// <summary>
		/// Constructor
		/// </summary>
		public requirementsCopyClosureBase():base()
		{
		}
	}
	#endregion
		
}

