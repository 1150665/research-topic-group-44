package requirements;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import pt.mei.edom.requirements.Model;
import pt.mei.edom.requirements.Requirement;
import pt.mei.edom.requirements.RequirementGroup;
import pt.mei.edom.requirements.RequirementsFactory;
import pt.mei.edom.requirements.RequirementsPackage;

public class GenerateDot {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		loadModel();
	}

	public static void loadModel() {
		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("requirements", new XMIResourceFactoryImpl());

		// Initialize the model
		RequirementsPackage.eINSTANCE.eClass();

		// Retrieve the default factory singleton
		RequirementsFactory factory = RequirementsFactory.eINSTANCE;

		// Obtain a new resource set
		ResourceSet resSet = new ResourceSetImpl();

		Resource resource = resSet.getResource(URI.createURI("instances/evento.requirements"), true);

		// now load the content.
		PrintWriter writer=null;
		try {
			resource.load(Collections.EMPTY_MAP);

			EObject root = resource.getContents().get(0);
			Model myModel=(Model)root;
			
			System.out.println("HERE IT IS: "+root.toString());
			
			//-----
			// Generate a plantUML file that represents the mind map diagram
			FileWriter w = new FileWriter("instances/requirements.puml");
	        writer = new PrintWriter(w);
	        
	        writer.println("@startuml");
	        writer.println("digraph xpto {");
	        
	        //ADICIONAR AQUI OS PARAMETROS
			// For the Requirement Group
			for (RequirementGroup e: myModel.getGroups()) {
				if (e instanceof RequirementGroup) {
					RequirementGroup r=(RequirementGroup)e;
					writer.println(myModel.getTitle() + " -> " + r.getName());
				}
			}
			
			//For the Model link to Groups
			for (RequirementGroup e: myModel.getGroups()) {
				if (e instanceof RequirementGroup) {
					RequirementGroup r=(RequirementGroup)e;
					writer.println(r.getName());
				}
			}
			
			// For the Requirement Groups nested
			for (RequirementGroup e : myModel.getGroups()) {
				if (e instanceof RequirementGroup) {
					RequirementGroup t = (RequirementGroup) e;
					for (RequirementGroup t2 : t.getChildren()) {
						writer.println(t.getName() + " -> " + t2.getName());
					}
				}
			}

			// For the requirements
//			for (RequirementGroup e : myModel.getGroups()) {
	//			if (e instanceof RequirementGroup) {
		//			RequirementGroup r = (RequirementGroup) e;
		//			for (Requirement r2 : r.getRequirements()) {
	//					writer.println(r.getName() + " -> " + r2.getTitle());
		//			}
		//		}
		//	}
			
			//resource.save(Collections.EMPTY_MAP);
			writer.println("}");
	        writer.println("@enduml");		
	        writer.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			writer.close();
		}
	}
}
