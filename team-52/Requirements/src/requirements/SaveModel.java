package requirements;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EValidator;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.ocl.common.OCLConstants;
import org.eclipse.ocl.pivot.internal.delegate.OCLDelegateDomain;
import org.eclipse.ocl.pivot.internal.delegate.OCLInvocationDelegateFactory;
import org.eclipse.ocl.pivot.internal.delegate.OCLSettingDelegateFactory;
import org.eclipse.ocl.pivot.internal.delegate.OCLValidationDelegateFactory;
import org.eclipse.ocl.pivot.model.OCLstdlib;

import pt.mei.edom.requirements.Model;
import pt.mei.edom.requirements.RequirementGroup;
import pt.mei.edom.requirements.RequirementsFactory;
import pt.mei.edom.requirements.RequirementsPackage;

public class SaveModel {

	// Example of how to use the EMF library
	public static void main(String[] args) {
      saveModel();
	}
	
	public static void initOCL() {
		//-----------------------------------------
		// Initialize Stand alone OCLInEcore
		// The first thing to do before using any code of the model
		String oclDelegateURI = OCLConstants.OCL_DELEGATE_URI;
		EOperation.Internal.InvocationDelegate.Factory.Registry.INSTANCE.put(oclDelegateURI,
			new OCLInvocationDelegateFactory.Global());
		EStructuralFeature.Internal.SettingDelegate.Factory.Registry.INSTANCE.put(oclDelegateURI,
			new OCLSettingDelegateFactory.Global());
		EValidator.ValidationDelegate.Registry.INSTANCE.put(oclDelegateURI,
			new OCLValidationDelegateFactory.Global());
		
		OCLDelegateDomain.initialize(null);
		
		org.eclipse.ocl.xtext.essentialocl.EssentialOCLStandaloneSetup.doSetup();

		OCLstdlib.install();
		//-------------		
	}
	
	public static void saveModel() {

		// Initialize OCL support
		initOCL();
		
		// Associate the "requirements" extension with the XMI resource format
		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("requirements", new XMIResourceFactoryImpl());
		
		// Initialize the model
		RequirementsPackage.eINSTANCE.eClass();

		// Retrieve the default factory singleton
        RequirementsFactory factory = RequirementsFactory.eINSTANCE;
        
        // Create a model
        Model myModel = factory.createModel();
        myModel.setTitle("EventModel");
        
        // Create the RequirementGroups
        RequirementGroup myEventRequirementGroup = factory.createRequirementGroup();
        myEventRequirementGroup.setName("Event");
        myEventRequirementGroup.setId("1");
        myEventRequirementGroup.setDescription("Event related requirements");
        
        RequirementGroup mySponsorRequirementGroup = factory.createRequirementGroup();
        mySponsorRequirementGroup.setName("Sponsor");
        mySponsorRequirementGroup.setId("2");
        mySponsorRequirementGroup.setDescription("Sponsor related requirements");
        
        RequirementGroup myUserRequirementGroup = factory.createRequirementGroup();
        myUserRequirementGroup.setName("User");
        myUserRequirementGroup.setId("3");
        myUserRequirementGroup.setDescription("User related requirements");
                
        RequirementGroup myNotificationRequirementGroup = factory.createRequirementGroup();
        myNotificationRequirementGroup.setName("Notification");
        myNotificationRequirementGroup.setId("4");
        myNotificationRequirementGroup.setDescription("Notification related requirements");
        
        // Add all nested Requirement groups
        myUserRequirementGroup.getChildren().add(myNotificationRequirementGroup);
        
        // Add all Requirement groups to the Model
        myModel.getGroups().add(myEventRequirementGroup);
        myModel.getGroups().add(mySponsorRequirementGroup);
        myModel.getGroups().add(myUserRequirementGroup);
        
		// Obtain a new resource set
        ResourceSet resSet = new ResourceSetImpl();
        
        // create a resource
        Resource resource = resSet.createResource(URI.createURI("instances/evento.requirements"));

        resource.getContents().add(myModel);
        
        // Add a call to validation...
        // See: https://stackoverflow.com/questions/8594212/how-to-programmatically-trigger-validation-of-emf-model
        // See: generated code for MindmapValidator.java
        // See: https://mattsch.com/2012/05/31/how-to-use-ocl-when-running-emf-standalone/
        // See: https://wiki.eclipse.org/OCL/OCLinEcore
        System.out.println("Diagnostic:");
        Diagnostic diag=Diagnostician.INSTANCE.validate(myModel);
        if (diag.getSeverity()!=Diagnostic.OK) {
        	System.out.println(diag.getMessage());
        	List<Diagnostic> l=diag.getChildren();
        	for (Diagnostic d: l) {
        		System.out.println(d.getMessage());
        	}
        }
        else {
        	System.out.println(" Everything seems fine :-)");
        }
        
        // now save the content.
        try {
            resource.save(Collections.EMPTY_MAP);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } 		
	}
}
